
mensajeEntrada="programa de generacion de tablas de multiplicar".capitalize()
#capitalize genera mensaje
print(mensajeEntrada.center(60,"="))
#center(60,"=") centrar titulo


#opcion=1 ejemplo para salir con letras
opcion ='s'

#while opcion!=0:
#ejemplo para salir con numero
while opcion!="n" or opcion!="N":
    #ingreasa los valores

    tablaInicio = int(input(chr(27) + "[1;36m" + "ingrese la tabla inicial :")) #(chr(27)+"[1;31m" para cambiar colores
    tablaFinal = int(input("ingrese la tabla final :"))

    while tablaFinal<tablaInicio:#añadir o comparar hacer comparaciones
        print("la tabla de inicio debe ser menor")
        tablaInicio = int(input("ingrese la tabla inicial :"))
        tablaFinal = int(input("ingrese la tabla final :"))


    rangoInicio=int(input("ingrese el rango inicial: "))
    rangoFinal=int(input("ingrese el rango final: "))
    while rangoFinal<rangoInicio:
        print("el rango inicial debe ser menor:")
        rangoInicio=int(input("ingrese el rango inicial: "))
        rangoFinal=int(input("ingrese el rango final: "))


    while tablaInicio<tablaFinal:
        for i in range(tablaInicio,tablaFinal+1):
            if i==4:
                print("la tabla {0} no la imprima por que no quiero",format(i))
                #continue serepite hasta llegar al final y volver a repetirse
                pass
            #pass pasa sin interunpir el proceso
            for j in range(rangoInicio,rangoFinal+1):
                #range es para recoger  hasta terminar el ciclo el + es para continuar
                if j==5:
                    print("mas de 5 no quiero : ")
                    break
                    #break es para romper un ciclo
                resultado=i*j
                #print("multiplicar ", i, " * ",j,"es igual a:",resultado)
                print("multiplicar %d *%d es iguala a: %d"%(i,j,resultado))

        tablaInicio=tablaInicio+1

    opcion =input("desea ejecutar nuevamente el proceso : \n"
        "s: continuar\n"
         "n :salir\n")
    if (opcion =="n") or (opcion =="N"):
        break

else:
    print("gracias por jugar")


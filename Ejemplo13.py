#3. Para ingresar a un curso de alto nivel, los candidatos deben realizar tres exámenes,
#A, B, C. Diseñe un algoritmo que reciba como entrada la edad y las notas de los tres
#exámenes. Calcule y despliegue el promedio de notas obtenido y un mensaje indicando
#si fue aceptado o no en el curso sabiendo que:
#Tener entre 15 y 18 años inclusive y obtener promedio de los exámenes mayor a 90.
#– Tener más de 18 años y que su promedio esté entre 80 y 90 inclusive.
#– Tener menos de 15 años y un promedio mayor o igual a 90, o un promedio mayor o
#igual a 80 pero no debe obtener menos de un 85 en la nota del examen c.
